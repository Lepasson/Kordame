﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kordame
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello My Sweet World!");

            string s = "Täna on " +
                (DateTime.Now.DayOfWeek == DayOfWeek.Saturday
                ||
                DateTime.Now.DayOfWeek == DayOfWeek.Sunday
                ? "Saun" : "Lihtsalt õlu") + ", niiet nautige!";
            Console.WriteLine(s);

            string k = "5"; //proovin kasutades stringe
            string m = "7";
            Console.WriteLine("Me võtame täna " + (k) + " tükki ");
            k = "7";
            Console.WriteLine("Me võtame täna " + (k) + " tükki ");
            Console.WriteLine("Me võtame täna " + (k) + " tükki ja siidreid võtame " + (m));
            Console.WriteLine();

            Console.WriteLine();

            int f = 5; //proovin kasutades int ehk täisarve
            int g = 7;
            Console.WriteLine("Me võtame täna " + (f) + " tükki ");
            Console.WriteLine("Me võtame täna " + (f) + " tükki ja siidreid võtame " + (g));

            Console.WriteLine();

            int a = 4_000_000; //alakriips ei tähenda midagi

            string nimi = "Diana";

            Console.WriteLine("Tere " + (nimi) + ", kuidas läheb?");
            Console.WriteLine("Mis päev täna on?");
            Console.WriteLine(DateTime.Now.DayOfWeek);

            a = 7;
            Console.WriteLine(++a); //võta järgmine
            Console.WriteLine(a++);
            Console.WriteLine(a);

            Console.WriteLine("Teeme väheke proovi asja!");
            Console.WriteLine();

            int b = 5;
            int c = 6;
            int d = 7;
            Console.WriteLine(b + c);
            Console.WriteLine();
            int C= c++;
            Console.WriteLine(b + c);
            Console.WriteLine(a + b + c);
            Console.WriteLine();
            Console.WriteLine((d/b) + "," +  (d%b));

            int o = 18 % 3;
            int p = 19 % 3;
            int r = 20 % 3;
            int t = 21 % 3;
            Console.WriteLine(o);
            Console.WriteLine(p);
            Console.WriteLine(r);
            Console.WriteLine(t);
            Console.WriteLine();

            Console.WriteLine(o==p);
            Console.WriteLine(o==r);
            Console.WriteLine(o==t);

            Console.WriteLine(); //seda proovi edasi, ei tulnud välja
            string s1 = "Diana Lepasson";
            String s2 = "Diana";
            String s3 = "Lepasson";
            String s4 = s2 + " " + s3;
            Console.WriteLine(s2 + s3);
            Boolean bk = s4 == s1;

            Console.WriteLine();
            Console.WriteLine();

            Console.Write("Anna üks arv: ");
            string vastus = Console.ReadLine();
            int v = int.Parse(vastus);
            Console.WriteLine("selle arvu ruut on " + (v * v).ToString());

            if (int.TryParse(vastus, out int w)) //täpsusta üle mis lõik see on, ei saanud päris täpselt aru toimivusest
                Console.WriteLine("selle arvu ruut on " + (w * w).ToString());
            else
                Console.WriteLine("see pole miski arv");

            char u = 'U';
            Console.WriteLine();


            Console.WriteLine();
            Console.WriteLine();

            /*stringidega töö*/
            Console.WriteLine();
            Console.WriteLine();

            string inimesenimi = "Diana Lepasson";
            //string failinimi = @"c:\\test\\failinimi.txt"; 
            //@ märk tähendab seda, et tagurpidi kaldkriipsu ei tõlgendataks kui käsku. Kaldkriips on lihtsalt märk siis
            string jutt = "Luts jutustab: „Kui Arno isaga kooli jõudis, olid tunnid juba alanud.“";
            string jutt2 = "Luts jutustab: \"Kui Arno \"";
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine(jutt);
            Console.WriteLine(jutt2);
            Console.WriteLine();
            Console.WriteLine();

            //peaks olema fail koo stekstiga olemas? Uuri järgi
            //jutt = System.IO.File.ReadAllText(failinimi);
            //console.WriteLine(jutt);

            Console.WriteLine();
            Console.WriteLine();

            string proov = "Kaia on tore tüdruk";
            foreach (string ss in proov.Split(' '))
            Console.WriteLine(ss);
            //forreach taga ei käi semikoolonit!

            Console.WriteLine();
            Console.WriteLine();

            //if lause (avaldis peab olema boolean tüüpi avaldis, ehk kahe võrdusmärgiga ==

            /*if (boolean-avaldis) lõppu ei panda semikoolonit!
            {
                //lausete jada, mis täidetakse
                //kui avaldis == true
                kui palju ja mis lauseid sees on ei oma mingit tähtsust
            } */

            /*if (boolean-avaldis)
            {
                //kui siin vahel on ainult üks lause, võib loogelised sulud ära jätta, muidu juhtub nii,
                et tulemuste lisamisel kehtib if ainult esimese kohta
                siis paigutada küik ühele reale, eeldab et rida ei lähe liiga pikaks, 
                maksimaalselt 80 tähemärki!
            } */

            /*if (boolean-avaldis)
            {
                //lausete blokk, mis täidetakse kui vastus on jah
            } */

            /*else (boolean-avaldis)
           {
               //lausete blokk, mis täidetakse kui vastus on jei
           } */

            int arv = 8;

            string kasArvJagubKolmega = (arv % 3 == 0) ? "Kolmega jaguv" : "Ei ole kolmega jaguv"; //ei juhtunud midagi, vaata üle


            Console.WriteLine();
            Console.WriteLine();

            //if else lause
            int muutuja1 = 33;

            if (muutuja1 % 2 == 0)
                Console.WriteLine("Arv on paarisarv");
            else
                Console.WriteLine("Arv ei ole paarisarv");

            Console.WriteLine();
            Console.WriteLine();

            Console.Write("Anna üks arv: ");
            string üksarv = Console.ReadLine();
            int x = int.Parse(üksarv);
            if (x % 2 == 0)
                Console.WriteLine("Arv on paarisarv");
            else
                Console.WriteLine("Arv ei ole paarisarv");

            Console.WriteLine();
            Console.WriteLine();

            //tingimustehe
            int arvuke = 9;
            string PaarisPaaritu = (arvuke % 2 == 0) ? "Paarisarv" : "Paaritu arv";
            Console.WriteLine(PaarisPaaritu);

            Console.WriteLine();
            Console.WriteLine();

            //tingimustehe vali ise arvuga
            Console.WriteLine(int.Parse(Console.ReadLine()) % 2 == 0 ? "Paarisarv" : "Paaritu arv");
            
            
            Console.WriteLine();
            Console.WriteLine();

            Console.Write("What color is the light?:");
            string color = Console.ReadLine().ToLower(); //muudab kirjutatu väikesteks tähtedeks, et toimiks

            if (color == "green")
                Console.WriteLine("Driver can drive a car.");
            else if (color == "yellow")
                Console.WriteLine("Driver has to be ready to stop the car or to start driving");
            else if (color == "red")
                Console.WriteLine("Driver has to be ready to stop the car and wait for the green light.");
            else
                Console.WriteLine("Käi jala");

            Console.WriteLine();
            Console.WriteLine();

            Console.Write("What color is the light?:");
            string color2 = Console.ReadLine().ToLower(); //kui keegi peaks eesti keeles kirjutama
            if (color2 == "green" || color2 == "roheline")
                Console.WriteLine("Driver can drive a car.");
            else if (color2 == "yellow" || color2 == "kollane")
                Console.WriteLine("Driver has to be ready to stop the car or to start driving");
            else if (color2 == "red" || color2 == "punane")
                Console.WriteLine("Driver has to be ready to stop the car and wait for the green light.");
            else
                Console.WriteLine("Käi jala");

            Console.WriteLine();
            Console.WriteLine();

            //variant kasutades || märki, st pole vaja ToLower käsku
            Console.Write("Variant 3. What color is the light?:");
            string color3 = Console.ReadLine(); 
            if (color3 == "green" || color3 == "GREEN")
                Console.WriteLine("Driver can drive a car.");
            else if (color3 == "yellow" || color3 == "YELLOW")
                Console.WriteLine("Driver has to be ready to stop the car or to start driving");
            else if (color3 == "red" || color3 == "RED")
                Console.WriteLine("Driver has to be ready to stop the car and wait for the green light.");
            else
                Console.WriteLine("Käi jala");

            //switchiga variant vt fotost


        }

    }
}
